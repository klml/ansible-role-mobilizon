Mobilizon
=========

Ce rôle installe et configure Mobilizon ainsi que :
- une base postgres
- le service systemd
- l'utilisateur admin
- le vhost nginx

Il a été testé seulement sur Debian 10.

## Playbook
Un exemple de playbook ressemble à ça:
~~~ ansible
- name: Mobilizon
  hosts: all
  remote_user: root
  vars:
    domain: mobilizon.mondomaine.fr
    instance_name: Mobilizon/test
    admin_email: contact@chez.moi
    admin_password: xxxxxxxxxxxxxxxxxxx
    dbpass: xxxxxxxxxxxxxxxx
    version_mobilizon: 1.0.3
  pre_tasks:
    - name: Stopping Service
      systemd:
        name: mobilizon
        state: stopped
  roles:
  - mobilizon
  post_tasks:
    - name: Activating Service
      systemd:
        name: mobilizon
        daemon_reload: yes
        enabled: yes
        state: started
~~~

Les variables par défaut se trouvent dans defaults/main.yml

## Tags
* user
* dependencies
* install 
* config
* create_db
* init_or_upgrade_db
* systemd
* admin_user
* nginx
* firewall

License
-------

GPLv3
